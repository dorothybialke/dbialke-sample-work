﻿using DVDLibrary.Data;
using DVDLibrary.Models;
using DVDLibrary.Models.Interfaces;
using DVDLibrary.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.BLL
{
    public class MovieManager
    {
        private IMovie _movieRepository;
        private IBorrower _borrowerRepository;

        public MovieManager(IMovie movieRepository, IBorrower borrowerRepository)
        {
            _movieRepository = movieRepository;
            _borrowerRepository = borrowerRepository;
        }

        public List<Movie> MovieList()
        {
            return _movieRepository.LoadMovies();
        }

        public List<Friend> FriendList()
        {
            return _borrowerRepository.LoadBorrower();
        }

        public SaveDVDResponse SaveMovie(Movie movie)
        {
            SaveDVDResponse response = new SaveDVDResponse();

            if (string.IsNullOrWhiteSpace(movie.Title))
            {
                response.Success = false;
                response.Message = "A title is needed for th DVD.";
                return response;
            }

            List<Movie> movieList = MovieList();

            //if (movieList.Any(m => m.MovieId == movie.MovieId))
            //{
            //    response.Success = false;
            //    response.Message = "You already have that DVD in your collection.";
            //    return response;
            //}

            _movieRepository.SaveDVD(movie);
            response.Success = true;
            response.SaveDVD = movie;
            return response;
        }

        public LookUpMovieResponse LookUpMovie(string title)
        {
            LookUpMovieResponse response = new LookUpMovieResponse();

            response.Result = _movieRepository.LoadMovies().FirstOrDefault(n => n.Title == title);

            if (response.Result == null)
            {
                response.Success = false;
                response.Message = $"{title} is not in collection.";
            }
            else
            {
                response.Success = true;
            }
            return response;
        }

        public DeleteDVDResponse DeleteMovie(Movie movie)
        {
            _movieRepository.DeleteDVD(movie.MovieId);
            DeleteDVDResponse response = new DeleteDVDResponse();
            response.Success = true;
            response.DeleteDVD = movie;
            return response;
        }
    }
}
