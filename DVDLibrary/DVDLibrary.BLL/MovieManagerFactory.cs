﻿using DVDLibrary.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.BLL
{
    public class MovieManagerFactory
    {
        public static MovieManager Create()
        {
            string mode = ConfigurationManager.AppSettings["Mode"].ToString();

            switch (mode)
            {
                case "InMemory":
                    return new MovieManager(new MovieRepository(), new BorrowerRepository());

                case "DataBase":
                    return new MovieManager(new MovieDataBase(), new BorrowerDataBase());

                default:
                    throw new Exception("Mode value in app config is not valid");
            }
        }
    }
}
