﻿using DVDLibrary.Models;
using DVDLibrary.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DVDLibrary.Data
{
    public class BorrowerDataBase : IBorrower
    {
        public static class Settings
        {
            private static string _connectionString;

            public static string ConnectionString
            {
                get
                {
                    if (string.IsNullOrEmpty(_connectionString))
                    {
                        _connectionString = ConfigurationManager.ConnectionStrings["DVDLibrary"].ConnectionString;
                    }
                    return _connectionString;
                }
            }
        }

        public List<Friend> LoadBorrower()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var friends = cn.Query<Friend>("Select * From Friend").ToList();

                return friends;
            }
        }
        public Friend GetBorrowerById(int? id)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var borrower = cn.Query<Friend>("Select Borrower From Friend Where BorrowerId = @Id", new { Id = id }).FirstOrDefault();

                return borrower;
            }
        }

        public Friend GetBorrowerByName(string name)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var borrower = cn.Query<Friend>("Select BorrowerId From Friend Where Borrower = @Name", new { Name = name }).FirstOrDefault();

                return borrower;
            }
        }

    }
}
