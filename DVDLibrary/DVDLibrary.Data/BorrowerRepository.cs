﻿using DVDLibrary.Models;
using DVDLibrary.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Data
{
    public class BorrowerRepository : IBorrower
    {
        private static List<Friend> friendList = new List<Friend>();

        static BorrowerRepository()
        {
            Friend f = new Friend
            {
                BorrowerId = 1,
                Borrower = "Chet",
               
            };
       
            friendList.Add(f);
            f = new Friend
            {
                BorrowerId = 2,
                Borrower = "Trigger",
           
            };
          
            friendList.Add(f);
            f = new Friend
            {
                BorrowerId = 3,
                Borrower = "Spot"
            };
            friendList.Add(f);
        }

        public List<Friend> LoadBorrower()
        {
            return friendList;
        }

        public void SaveBorrower(Friend friend)
        {
            if (friend.BorrowerId <= 0)
            {
                friend.BorrowerId = GetNextBorrowerIdNumber();
                friendList.Add(friend);
            }
            else //update
            {
                foreach (var item in friendList)
                {
                    if (item.BorrowerId == friend.BorrowerId)
                    {
                        item.Borrower = friend.Borrower;
                    }
                }
            }
        }

        public int GetNextBorrowerIdNumber()
        {
            int MaxID = 0;

            foreach (Friend item in friendList)
            {
                if (item.BorrowerId > MaxID)
                {
                    MaxID = item.BorrowerId;
                }
            }
            return MaxID + 1;
        }
    }
}


