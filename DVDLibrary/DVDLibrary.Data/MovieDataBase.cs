﻿using DVDLibrary.Models;
using DVDLibrary.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using Dapper;


namespace DVDLibrary.Data
{
    public class MovieDataBase : IMovie
    {
        public static class Settings
        {
            private static string _connectionString;

            public static string ConnectionString
            {
                get
                {
                    if (string.IsNullOrEmpty(_connectionString))
                    {
                        _connectionString = ConfigurationManager.ConnectionStrings["DVDLibrary"].ConnectionString;
                    }
                    return _connectionString;
                }
            }
        }

        public void DeleteDVD(int id)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var affectedrow = cn.Execute("Delete From Movie Where MovieId = @Id", new { Id = id });
                return;
            };
        }

        public List<Movie> LoadMovies()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var movies = cn.Query<Movie>("Select * From Movie").ToList();

                foreach (var item in movies)
                {
                    var BorrowerDataBase = new BorrowerDataBase();
                    Friend friend = BorrowerDataBase.GetBorrowerById(item.BorrowerId);
                    item.Borrower = friend;
                }
                return movies;
            }
        }

        public void SaveDVD(Movie movie)
        {
            if (movie.MovieId > 0)
            {
                Edit(movie);
            }
            else
            {
                movie = Insert(movie);
            }
        }

        private Movie Insert(Movie movie)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                string sql = "INSERT INTO Movie (Title, ReleaseDate, MPAARating, DirectorsName," +
                    "Studio, UserRating, UserNotes, Actors) Values(" +
                    "@Title, @ReleaseDate, @MPAARating, @DirectorsName, @Studio, @UserRating, @UserNotes," +
                    "@Actors);" +
                    "SELECT CAST(SCOPE_IDENTITY() As int)";
                var parms = new
                {
                    Title = movie.Title,
                    ReleaseDate = movie.ReleaseDate == DateTime.MinValue ? default(DateTime?) : movie.ReleaseDate,
                    MPAARating = movie.MPAARating,
                    DirectorsName = movie.DirectorsName,
                    Studio = movie.Studio,
                    UserRating = movie.UserRating,
                    UserNotes = movie.UserNotes,
                    Actors = movie.Actors
                };
                movie.MovieId = cn.Query<int>(sql, parms).First();
                return movie;
            }
        }

        private Movie Edit(Movie movie)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                string sql = @"
                update movie set 
                Title = @Title,
                ReleaseDate =@ReleaseDate,
                MPAARating = @MPAARating,
                DirectorsName = @DirectorsName,
                Studio = @Studio,
                UserRating = @UserRating,
                UserNotes = @UserNotes,
                Actors = @Actors,
                BorrowerId = @BorrowerId,
                LentDate = @LentDate,
                ReturnDate = @ReturnDate 
                where MovieId = @MovieId
                ";

                var BorrowerDataBase = new BorrowerDataBase();
                Friend friend = BorrowerDataBase.GetBorrowerByName(movie.Borrower.Borrower);
                if (friend != null)
                {
                    movie.Borrower.BorrowerId = friend.BorrowerId;
                }
                
                var parms = new
                {
                    MovieId = movie.MovieId,
                    Title = movie.Title,
                    ReleaseDate = movie.ReleaseDate == DateTime.MinValue ? default(DateTime?) : movie.ReleaseDate,
                    MPAARating = movie.MPAARating,
                    DirectorsName = movie.DirectorsName,
                    Studio = movie.Studio,
                    UserRating = movie.UserRating,
                    UserNotes = movie.UserNotes,
                    Actors = movie.Actors,
                    BorrowerId = movie.Borrower.BorrowerId <= 0 ? default(int?) : movie.Borrower.BorrowerId,
                    LentDate = movie.LentDate == DateTime.MinValue ? default(DateTime?) : movie.LentDate,
                    ReturnDate = movie.ReturnDate == DateTime.MinValue ? default(DateTime?) : movie.ReturnDate
                };
                cn.Execute(sql, parms);
                return movie;
            }
        }
    }
}
