﻿using DVDLibrary.Models;
using DVDLibrary.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Data
{
    public class MovieRepository : IMovie
    {
        private static List<Movie> movieList = new List<Movie>();

        private static BorrowerRepository br = new BorrowerRepository();
        
        static MovieRepository()
        {
            var friendList = br.LoadBorrower();

            Movie m = new Movie
            {
                MovieId = 1,
                Title ="Raising Arizona",
                ReleaseDate = new DateTime(1997,04,17),
                MPAARating = Ratings.PG13,
                DirectorsName = "Joel Coen, Ethen Coen",
                Studio = "20th Century Fox",
                UserRating= 8,
                UserNotes = "Nicholas Cage and Holly Hunter were outstanding.",
                Actors = "Nicholas Cage, Holly Hunter, Trey Wilson",
                LentDate= new DateTime (2016,6,2),
                Borrower= friendList[0]
            };
         

            movieList.Add(m);
            m = new Movie
            {
                MovieId = 2,
                Title = "The Big Lebowski",
                ReleaseDate = new DateTime(1998,03,06),
                MPAARating = Ratings.R,
                DirectorsName = "Joel Coen, Ethen Coen",
                Studio = "Gramercy Pictures",
                UserRating = 9,
                UserNotes = "Crazy story but a fun watch.",
                Actors = "John Goodman, Jeff Bridges, Julianne Moore",
                ReturnDate = new DateTime (2015,10,5)
            };
            movieList.Add(m);
            m = new Movie
            {
                MovieId = 3,
                Title = "True Grit",
                ReleaseDate = new DateTime(2010,12,22),
                MPAARating = Ratings.PG13,
                DirectorsName = "Joel Coen, Ethen Coen",
                Studio = "Paramount Pictures",
                UserRating = 9,
                UserNotes = "Got to love a good western with a powerful girl.",
                Actors = "Jeff Bridges, Matt Damon, Hailee Steinfeld",
                LentDate = new DateTime(2016,9,4),
                Borrower = friendList[1]
            };

            movieList.Add(m);
            m = new Movie
            {
                MovieId = 4,
                Title = "The Artist",
                ReleaseDate = new DateTime(2011,1,20),
                MPAARating = Ratings.PG13,
                DirectorsName = "Michel Hazanavicius",
                Studio = "Studio 37",
                UserRating = 10,
                UserNotes = "Very creative and it has a dog as a sidekick, that is always awesome.",
                Actors = "Jean Dujardin, Berenice Bejo, John Goodman"
            };
            movieList.Add(m);
        }
        
        public List<Movie> LoadMovies()
        {
            return movieList;

        }
        public List<Friend> Friend()
        {
            throw new NotImplementedException();
        }

        public Movie LookUpMoviebyTitle(string title)
        {
            return LoadMovies().FirstOrDefault(m => m.Title == title);
        }
       

        public void SaveDVD(Movie movie)
        {
            if (movie.MovieId <= 0) //add
            {
                movie.MovieId = GetNextMovieIdNumber();
                movieList.Add(movie);
            }
            else //update
            {
                foreach (var item in movieList)
                { 
                    if (item.MovieId == movie.MovieId)
                    {
                        item.Title = movie.Title;
                        item.Studio = movie.Studio;
                        item.DirectorsName = movie.DirectorsName;
                        item.UserRating = movie.UserRating;
                        item.MPAARating = movie.MPAARating;
                        item.ReleaseDate = movie.ReleaseDate;
                        item.Actors = movie.Actors;
                        item.UserNotes = movie.UserNotes;
                        item.LentDate = movie.LentDate;
                        item.ReturnDate = movie.ReturnDate;
                        item.Borrower = movie.Borrower;
                    }
                }
            }
        }
        public int GetNextMovieIdNumber()
        {
            int MaxID = 0;

            foreach (Movie item in movieList)
            {
                if (item.MovieId > MaxID)
                {
                    MaxID = item.MovieId;
                }
            }
            return MaxID + 1;
        }

        public void DeleteDVD(int id)
        {

            movieList.Remove(movieList.FirstOrDefault(m => m.MovieId == id));

        }

       
    }
}
