﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models
{
    public class Friend
    {
        public int BorrowerId { get; set; }
        public string Borrower { get; set; }
        public override string ToString()
        {
            return Borrower;
        }
    }
}
