﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models.Interfaces
{
    public interface IMovie
    {
        List<Movie> LoadMovies();

        void SaveDVD(Movie movie);

        void DeleteDVD(int id);
    }
}
