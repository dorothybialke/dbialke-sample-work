﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models
{
    public class Movie
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public DateTime ReleaseDate {get; set;}
        public Ratings MPAARating { get; set; }
        public string DirectorsName { get; set; }
        public string Studio { get; set; }
        public int UserRating { get; set; }
        public string UserNotes { get; set; }
        public string Actors { get; set; }
        public DateTime LentDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public int? BorrowerId { get; set; }
        public Friend Borrower { get; set; }

    }
}
