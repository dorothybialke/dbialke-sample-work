﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models
{
    public enum Ratings
    {
        G=1,
        PG,
        PG13,
        R,
        NR
    }
}
