﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models.Responses
{
    public class BorrowerResponse : Response
    {
        public Friend Borrower { get; set; }
    }
}
