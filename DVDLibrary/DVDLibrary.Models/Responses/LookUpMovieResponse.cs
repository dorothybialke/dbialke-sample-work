﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models.Responses
{
    public class LookUpMovieResponse : Response
    {
        public Movie Result { get; set; }
    }
}
