﻿using DVDLibrary.BLL;
using DVDLibrary.Data;
using DVDLibrary.Models;
using DVDLibrary.Models.Interfaces;
using DVDLibrary.Models.Responses;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Tests
{
    [TestFixture]
    public class DVDTests
    {
        [TestCase("Raising Arizona", true)]
        [TestCase("The Artist", true)]
        [TestCase("Animal House", false)]
        [TestCase("", false)]
        public void CanLoadDVDLibrary(string title, bool expected)
        {
            MovieManager manager = MovieManagerFactory.Create();
            LookUpMovieResponse response = manager.LookUpMovie(title);
            Assert.AreEqual(expected, response.Success);
        }

        [TestCase("Fargo", true)]
        [TestCase("The Artist", false)]
        [TestCase("", false)]
        public void SaveNewMovie(string title, bool expected)
        {
            MovieManager manager = MovieManagerFactory.Create();
            Movie mov = new Movie();
            mov.Title = title;
            SaveDVDResponse response = manager.SaveMovie(mov);

            Assert.AreEqual(expected, response.Success);
        }

    }

    
}
