﻿using DVDLibrary.BLL;
using DVDLibrary.Data;
using DVDLibrary.Models;
using DVDLibrary.Models.Interfaces;
using DVDLibrary.WebUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DVDLibrary.WebUI.Controllers
{
    public class HomeController : Controller
    {
        MovieManager manager = MovieManagerFactory.Create();

        // GET: Home
        public ActionResult Index()
        {
            MovieVM vm = new MovieVM();
            vm.Movie = manager.MovieList();
            return View(vm);
          
        }
    }
}