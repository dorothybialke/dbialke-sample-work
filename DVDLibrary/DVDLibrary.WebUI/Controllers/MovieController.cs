﻿using DVDLibrary.BLL;
using DVDLibrary.Models;
using DVDLibrary.Models.Interfaces;
using DVDLibrary.WebUI.ViewModels;
using DVDLibrary.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DVDLibrary.WebUI.Controllers
{
    public class MovieController : Controller
    {
        MovieManager manager = MovieManagerFactory.Create();
        // GET: Movie
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ManageDVD()
        {
            MovieVM vm = new MovieVM();
            vm.Movie = manager.MovieList();
            return View(vm);
        }
        [HttpPost]
        public ActionResult ManageDVD(string title)
        {
            return View();
        }

        [HttpGet]
        public ActionResult EditDVD(string id)
        {
            List<Movie> d = manager.MovieList();
            var mToEdit = d.FirstOrDefault(m => m.Title == id);
            return View(mToEdit);

        }
        [HttpPost]
        public ActionResult EditDVD(Movie m)
        {
            List<Movie> d = manager.MovieList();
            manager.SaveMovie(m);
            return RedirectToAction("ManageDVD");
        }

        [HttpGet]
        public ActionResult AddDVD()
        {
            return View();

        }
        [HttpPost]
        public ActionResult AddDVD(Movie m)
        {
            List<Movie> d = manager.MovieList();
            manager.SaveMovie(m);
            return RedirectToAction("ManageDVD");
        }

        [HttpGet]
        public ActionResult DeleteDVD(string id)
        {
            List<Movie> d = manager.MovieList();
            var mToDelete = d.FirstOrDefault(m => m.Title == id);
            return View(mToDelete);
        }
        [HttpPost]
        public ActionResult DeleteDVD(Movie model)
        {
            manager.DeleteMovie(model);
            return RedirectToAction("ManageDVD");
        }

        [HttpGet]
        public ActionResult Friend()
        {
            FriendVM vm = new FriendVM();
            vm.Friend = manager.FriendList();
            return View(vm);

        }
        [HttpPost]
        public ActionResult Friend(string b)
        {
            
            return View();
        }


    }
}