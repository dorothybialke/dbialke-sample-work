﻿//$(".mySaveModal").click(function () {
//    alert("It works");
//});



$(".dvdbutton").click(function () {
    var $this = $(this);
    var title = $this.data("title");

    $("#DvdTitle").val(title);
    $("#DvdStudio").val($this.data("studio"));
    $("#DvdDirectorsName").val($this.data("directors"));
    $("#DvdUserRating").val($this.data("userrating"));
    $("#DvdMPAA").val($this.data("mpaarating"));
    $("#DvdRelease").val($this.data("releasedate"));
    $("#DvdActors").val($this.data("actors"));
    $("#DvdUserNotes").val($this.data("usernotes"));
    $("#DvdFriend").val($this.data("friend"));
    $("#DvdLentDate").val($this.data("lentdate"));
    $("#DvdReturnDate").val($this.data("returndate"));
});

function myFunction() {


    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (var i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}