﻿using DVDLibrary.Models;
using DVDLibrary.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace DVDLibrary.WebUI.ViewModels
{
    public class FriendVM
    {
        public List<Friend> Friend { get; set; }
        
    }
}